#install headers
install(
  FILES
    logging.hh
  DESTINATION
    ${CMAKE_INSTALL_INCLUDEDIR}/dune
  )

add_subdirectory(logging)
