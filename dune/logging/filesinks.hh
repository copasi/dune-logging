// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_FILESINKS_HH
#define DUNE_LOGGING_FILESINKS_HH

#include <string>
#include <string_view>

#include <fmt/posix.h>

#include <dune/logging/patternformatsink.hh>

namespace Dune::Logging {

  /**
   * \addtogroup logging
   * \{
   */

  //! Sink for logging data to a file.
  /**
   * FileSink opens a given file and logs all data to that file. It is typically configured by
   * specifying the sink type "file" in the logging configuration. It supports the following
   * configuration keys:
   *
   * | Key     | Description                                                  |
   * |-------- |--------------------------------------------------------------|
   * | file    | The name of the log file. See below for further information. |
   * | mode    | "truncate" or "append".                                      |
   * | pattern | The pattern for the log message, see PatternFormatSink.      |
   *
   * When running programs in parallel, each rank will write to its own file: the filename will
   * automatically be prepended with the 0-padded rank of each process. Alternatively, if the
   * filename contains a "{}" somewhere, this placeholder will be replaced by the 0-padded rank
   * value. In this case, the replacement also happens when running a sequential program.
   */
  class FileSink
    : public PatternFormatSink
  {

  public:

    FileSink(
      std::string_view name,
      LogLevel level,
      std::size_t widest_logger,
      const std::string& file_name,
      const std::string& mode
      )
      : PatternFormatSink(name,level,widest_logger)
      , _file_name(file_name)
      , _file(file_name,mode)
    {}

    void process(const LogMessage& msg) override
    {
      PatternFormatSink::Arguments args(msg,*this);
      _file.vprint(fmt::to_string_view(pattern()), args);
    }

  private:

    std::string _file_name;
    fmt::buffered_file _file;

  };

  /**
   * \}
   */

} // namespace Dune::Logging

#endif // DUNE_LOGGING_FILESINKS_HH
