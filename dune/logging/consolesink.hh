// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_CONSOLESINK_HH
#define DUNE_LOGGING_CONSOLESINK_HH

#include <cstdio>
#include <string_view>

#include <dune/logging/patternformatsink.hh>

namespace Dune::Logging {

  /**
   * \addtogroup logging
   * \{
   */

  //! A sink for writing to the console, typically stdout or stderr.
  /**
   * The logging system wraps stdout and stderr in two instances of this class, which are available
   * as Logging::cout() and Logging::cerr();
   */
  class ConsoleSink
    : public PatternFormatSink
  {

  public:

    ConsoleSink(
      std::string_view name,
      std::FILE* stream,
      const std::string& pattern,
      LogLevel level,
      std::size_t widest_logger
      )
      : PatternFormatSink(name,level,widest_logger)
      , _stream(stream)
    {
      setPattern(pattern);
    }

    void process(const LogMessage& msg) override
    {
      PatternFormatSink::Arguments args(msg,*this);
      fmt::vprint(_stream,pattern(),args);
    }

  private:

    std::FILE* _stream;

  };

  /**
   * \}
   */

} // end namespace Dune::Logging

#endif // DUNE_LOGGING_CONSOLESINK_HH
