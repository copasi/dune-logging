// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include "config.h"

#include <dune/logging/sink.hh>

namespace Dune::Logging {

  Sink::~Sink()
  {}

  void NullSink::process(const LogMessage &)
  {}

} // namespace Dune::Logging
