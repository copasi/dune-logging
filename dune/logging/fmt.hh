// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_FMT_HH
#define DUNE_LOGGING_FMT_HH

#include <optional>
#include <type_traits>

#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/chrono.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>
#include <fmt/chrono.h>

#include <dune/logging/checks.hh>

// disable clang warning about the compiler extension used in our user-defined _fmt literal
#ifdef __clang__
#pragma GCC diagnostic ignored "-Wgnu-string-literal-operator-template"
#endif

namespace Dune::Logging {

  /**
   * \addtogroup fmt {fmt} Extensions
   * \brief Additional Dune-specific infrastructure for `{fmt}`-based string formatting
   * \ingroup logging
   * @{
   */

  //! A compile-time string that allows constructing constexpr string_views of the captured string.
  /**
   * This class is required for the compile-time validation of _fmt format strings and produced by
   * the _fmt literal when DUNE_LOGGING_CHECK_FORMAT_STRINGS is enabled.
   */
  template<typename Char, Char... chars>
  struct format_string
    : fmt::compile_string
  {

#ifndef DOXYGEN

    // required by the fmt library
    using char_type = Char;

    // static null-terminated string constructed from the literal
    constexpr static char_type string[] = { chars..., 0 };

#endif

    //! Constructs a constexpr fmt::basic_string_view used by fmt to perform compile-time format string validation.
    constexpr operator fmt::basic_string_view<char_type>() const
    {
      return { string, sizeof...(chars) };
    }

    //! Constructs a constexpr std::string_view containing the literal.
    constexpr operator std::string_view() const
    {
      return { string, sizeof...(chars) };
    }

    constexpr friend fmt::basic_string_view<char_type> to_string_view(format_string s)
    {
      return { s };
    }


  };

  //! A special string_view that lets us ensure that users always use the _fmt literal for format strings.
  /**
   * This class is produced by the _fmt literal when DUNE_LOGGING_CHECK_FORMAT_STRINGS is disabled.
   */
  struct format_string_view
    : std::string_view
  {

#ifndef DOXYGEN

    constexpr format_string_view() = default;

    constexpr format_string_view(const char* s, std::size_t size)
      : std::string_view(s,size)
    {}

#endif

  };

  //! Type trait for detecting whether a given type is a format_string.
  template<typename T>
  struct is_format_string
    : public std::false_type
  {};

#ifndef DOXYGEN

  template<typename Char, Char... chars>
  struct is_format_string<format_string<Char,chars...>>
    : public std::true_type
  {};

#endif

  //! boolean value indicating whether T is a format_string.
  template<typename T>
  constexpr bool is_format_string_v = is_format_string<T>::value;


  /**
   * \brief A fixed-type holder for a factory that produces its value only when mentioned in a fmt
   * format string.
   *
   * This is a useful construct for classes that optionally offer a number of expensive-to-compute
   * arguments to users specifying a format string, see e.g. PatternFormatSink.
   *
   * The {fmt} framework will detect format string arguments of this type, invoke the factory and
   * format the result of that function call.
   *
   * If the template parameter `cached` is set to `true`, the result of the function call will be
   * cached.
   */
  template<typename Factory, bool cached = false>
  struct LazyFormatArgument
  {

    LazyFormatArgument(Factory f)
      : factory(std::move(f))
    {}

    auto operator()() const
    {
      return factory();
    }

    mutable Factory factory;
    };

#ifndef DOXYGEN

  // caching version
  template<typename Factory>
  struct LazyFormatArgument<Factory,true>
  {

    LazyFormatArgument(Factory f)
      : factory(std::move(f))
    {}

    auto operator()() const
    {
      if (not cache)
        cache.emplace(factory());
      return *cache;
    }

    mutable Factory factory;
    mutable std::optional<decltype(factory())> cache;

  };

#endif

  /**
   * \brief Helper function to make {fmt} lazily evaluate the passed-in invocable object.
   *
   * This function takes an invocable that can be called without any arguments and returns a value that
   * should be formatted by {fmt}. It delays execution of the function until the value is actually formatted.
   * This can be used to avoid expensive calculations in the fast path of a log call, where the actual logging
   * does not take place.
   *
   * \note The function will be called every time the argument gets formatted, which can happen multiple times
   *       if there is more than one sink configured. If you want to make sure that the function is called at
   *       most once, use lazyCached() instead.
   *
   */
  template<typename Factory>
  LazyFormatArgument<Factory> lazy(Factory f)
  {
    return LazyFormatArgument<Factory>(std::move(f));
  }

  /**
   * \brief Helper function to make {fmt} lazily evaluate the passed-in invocable object and cache the result.
   *
   * This function takes an invocable that can be called without any arguments and returns a value that
   * should be formatted by {fmt}. It delays execution of the function until the value is actually formatted.
   * This can be used to avoid expensive calculations in the fast path of a log call, where the actual logging
   * does not take place.
   *
   * \note The function will be called at most once, the first time the argument is used. The result of the call
   *       will then be cached and returned unchanged if the argument is used again. If you don't want or need
   *       the caching, use lazy() instead.
   */
  template<typename Factory>
  LazyFormatArgument<Factory,true> lazyCached(Factory f)
  {
    return LazyFormatArgument<Factory,true>(std::move(f));
  }

} // end namespace Dune::Logging


#ifndef DOXYGEN

namespace fmt {

  template <typename Factory, bool cached, typename Char>
  struct formatter<Dune::Logging::LazyFormatArgument<Factory,cached>,Char>
    : public formatter<
        std::conditional_t<
          fmt::
#if FMT_VERSION < 70000
          internal
#else
          detail
#endif
          ::is_string<
            std::decay_t<decltype(std::declval<Factory>()())>
            >::value,
          fmt::basic_string_view<Char>,
          std::decay_t<decltype(std::declval<Factory>()())>
          >,
        Char
        >
  {

    using Arg    = Dune::Logging::LazyFormatArgument<Factory,cached>;
    using Result = std::decay_t<decltype(std::declval<Factory>()())>;
    using Base   = formatter<
      std::conditional_t<
        fmt::
#if FMT_VERSION < 70000
        internal
#else
        detail
#endif
        ::is_string<Result>::value,
        fmt::basic_string_view<Char>,
        Result
        >,
      Char>;

    // we just inherit the parsing from the original formatter

    template <typename FormatContext>
    auto format(const Arg& arg, FormatContext& ctx) {
      return Base::format(arg(),ctx);
    }

  };

}

#endif // DOXYGEN

namespace Dune::Literals {

#ifdef DOXYGEN

  //! String literal operator for marking {fmt} format strings.
  /**
   * The string literator `_fmt` must be used when writing format strings for passing
   * to fmt, in particular to the logging framework. To turn a normal string literal
   * into a format string literal, just append `_fmt` to the literal:
   *
   * ~~~
   * "This is a format string with {} and {:6.3e} some data"_fmt
   * ~~~
   *
   * In order to use the custom string literal, you need to import the namespace Dune::Literals
   * with
   *
   * ~~~
   * using namespace Dune::Literals;
   * ~~~
   *
   * This will only import the literal (and possibly other Dune-related literals) without polluting
   * your scope with other Dune-related names.
   *
   * \note You do not need to import this namespace if your code lives in namespace Dune or a nested
   *       namespace, as the literal is also imported into namespace Dune.
   *
   * \sa DUNE_LOGGING_CHECK_FORMAT_STRINGS
   */
  template<typename Char, Char... chars>
  constexpr Dune::Logging::format_string<Char,chars...> operator""_fmt()
  {
    return {};
  }


#else

#if DUNE_LOGGING_CHECK_FORMAT_STRINGS

  template<typename Char, Char... chars>
  constexpr Dune::Logging::format_string<Char,chars...> operator""_fmt()
  {
    return {};
  }

#else

  constexpr Dune::Logging::format_string_view operator""_fmt(const char* string, std::size_t size)
  {
    return { string, size };
  }

#endif

#endif // DOXYGEN

} // end namespace Dune::Literals


namespace Dune {

  using Dune::Literals::operator""_fmt;

}

/**
 * @} fmt
 */

#endif // DUNE_LOGGING_FMT_HH
