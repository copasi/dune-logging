// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_TYPE_TRAITS_HH
#define DUNE_LOGGING_TYPE_TRAITS_HH

#include <dune/common/typetraits.hh>
#include <dune/common/std/type_traits.hh>

namespace Dune::Std {

  template <typename T>
  constexpr bool to_true_type_v = to_true_type<T>::value;

} // end namespace Dune::Std

#endif // DUNE_LOGGING_TYPE_TRAITS_HH
