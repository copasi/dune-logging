Licensing Information
=====================

Copyright holders
-----------------

| Year       | Name                       |
|------------|----------------------------|
| 2019       | Steffen Müthing            |

License
-------

The dune-logging library, headers and test programs are copyrighted free software. You
can use, modify and/or redistribute it under the terms of either one of the three
following licenses:

* The BSD 2-clause license. You can find a copy in the file [BSD-2][7].

* The GNU Lesser General Public License as published by the Free Software
  Foundation, either Version 3 of the license or (at your option) any later
  version. You can find a copy of the GNU Lesser General Public License, Version
  3, in the files [GPL-3][1] and [LGPL-3][2] or at http://www.gnu.org/licenses/lgpl-3.0.

* Version 2 of the GNU General Public License as published by the Free Software
  Foundation, with the following special exception for linking and compiling
  against the dune-logging library, the so-called "runtime exception":

>   As a special exception, you may use the dune-logging source files as part of a
>   software library or application without restriction.  Specifically, if other
>   files instantiate templates or use macros or inline functions from one or
>   more of the dune-logging source files, or you compile one or more of the dune-logging
>   source files and link them with other files to produce an executable, this
>   does not by itself cause the resulting executable to be covered by the GNU
>   General Public License.  This exception does not however invalidate any
>   other reasons why the executable file might be covered by the GNU General
>   Public License.

  This license is intended to be similar to the GNU Lesser General Public
  License, Version 2, which by itself isn't suitable for a template library. You
  can find a copy of the GNU General Public License, Version 2, in the file
  [GPL-2][3] or at http://www.gnu.org/licenses/gpl-2.0.

External Libraries
------------------

dune-logging contains the external [{fmt}][4] library in the subdirectory `vendor/fmt`,
which is checked out as a Git submodule. This library is by default obtained from
a [mirror][5] of the original repository hosted on our GitLab. {fmt} is distributed
as part of dune-logging if the build system cannot find an installed version of {fmt}.

{fmt} is licensed under the BSD 2-clause license. For further information about the license and
copyright information, see [vendor/fmt/README.rst][5] and [vendor/fmt/LICENSE.rst][6]

Links
-----

[1]: GPL-3
[2]: LGPL-3
[3]: GPL-2
[4]: https://fmtlib.net
[5]: vendor/fmt/README.rst
[6]: vendor/fmt/LICENSE.rst
[7]: BSD-2
